package nl.wur.ssb.curation;

import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.time.LocalDate;
import java.time.LocalDateTime;

import org.purl.ontology.bibo.domain.AcademicArticle;
import org.purl.ontology.bibo.domain.Issue;
import org.purl.ontology.bibo.domain.Journal;
import org.w3.ns.prov.domain.Organization;
import org.w3.ns.prov.domain.Person;

import life.gbol.domain.AnnotationLinkSet;
import life.gbol.domain.Chromosome;
import life.gbol.domain.Citation;
import life.gbol.domain.Curator;
import life.gbol.domain.Database;
import life.gbol.domain.ExactPosition;
import life.gbol.domain.FeatureProvenance;
import life.gbol.domain.GBOLDataSet;
import life.gbol.domain.Gene;
import life.gbol.domain.ManualAnnotationActivity;
import life.gbol.domain.Note;
import life.gbol.domain.Organism;
import life.gbol.domain.ProvenanceAnnotation;
import life.gbol.domain.Region;
import life.gbol.domain.StrandPosition;
import life.gbol.domain.StrandType;
import life.gbol.domain.XRef;
import life.gbol.domain.XRefProvenance;
import nl.wur.ssb.RDFSimpleCon.api.Domain;

public class Main
{
  public static void main(String args[]) throws Exception
  {
    new Main();
  }
  public Main() throws Exception
  {   
    //Create the domain object holding the RDF data within the memory
    Domain domain = new Domain("");
         
    //Create a the main chromoson for the example
    Chromosome mainDnaObject = domain.make(Chromosome.class,"http://example.com/mainDnaObject");
    mainDnaObject.setSequence("ATAGTACCATCCACGACGTACTAGCATAGAGTAGGATTTTAAGTTCCAAGTTTTCCAAACTGGTAAGTGCCAAG");
    mainDnaObject.setSha384(checksum(mainDnaObject.getSequence()));
    
    //Create a organism
    Organism organism = domain.make(Organism.class,"http://example.com/someOrganism");
    organism.setScientificName("A example bacterium");
    mainDnaObject.setOrganism(organism);
    mainDnaObject.setStrandType(StrandType.DoubleStrandedDNA);
    
    //Create A gene with a location
    Gene aGene = domain.make(Gene.class,"http://example.com/oneGene");
    aGene.setLocusTag("Elovl2");
    //Create a location for the gene
    Region location = domain.make(Region.class,"http://example.com/location1");
    //Create and set a start position
    ExactPosition start = domain.make(ExactPosition.class,"http://example.com/posstart");
    start.setPosition(10L);
    //Create and set a end position
    ExactPosition end = domain.make(ExactPosition.class,"http://example.com/posend");
    end.setPosition(40L);
    //Attach the positions to the interval location
    location.setBegin(start);
    location.setEnd(end);
    location.setStrand(StrandPosition.ForwardStrandPosition);
    //Attach the location to the gene
    aGene.setLocation(location);
    
    //----------- The creation of the manual curation
    
    //Create organisation
    Organization organisation = domain.make(Organization.class,"http://example.com/wur");
    organisation.setLegalName("Wageningen University");
    organisation.setName("Wageningen University");
    //Create a curator, which acted on behalve of wageningen university
    Curator jesse = domain.make(Curator.class,"http://example.com/jesse");
    jesse.setOrcid("http://orcid.org/123123-1123");
    jesse.setName("Jesse van Dam");
    jesse.setActedOnBehalfOf(organisation);
    //Create a activity, which hold the time of annotation (start & end are the same)
    //This is the annotation activity that resulted in the creation of the references in this example
    ManualAnnotationActivity annotationActivity = domain.make(ManualAnnotationActivity.class,"http://example.com/annotationActivity1");
    LocalDateTime time = LocalDateTime.now();
    annotationActivity.setStartedAtTime(time);
    annotationActivity.setEndedAtTime(time);
    
    //Create a database that is referenced
    Database ncbiGeneDatabase = domain.make(Database.class,"http://example.com/databaseNCBIgene");
    ncbiGeneDatabase.setId("NG");
    ncbiGeneDatabase.setUriSpace("http://ncbi.com/gene");
    ncbiGeneDatabase.setUriLookupEndpoint("https://www.ncbi.nlm.nih.gov/gene/?term=");
    
    //Create an other database 
    Database ncbiProteinDatabase = domain.make(Database.class,"http://example.com/databaseNCBIprotein");
    ncbiProteinDatabase.setId("NP");
    ncbiProteinDatabase.setUriSpace("http://ncbi.com/protein");
    ncbiProteinDatabase.setUriLookupEndpoint("https://www.ncbi.nlm.nih.gov/protein/?term=");
    
    //Create yet another database
    Database goDatabase = domain.make(Database.class,"http://example.com/databaseNCBIgene");
    goDatabase.setId("GO");
    goDatabase.setUriSpace("http://geneontology.org/");
    goDatabase.setUriLookupEndpoint("https://geneontology.org/?term=");
    
    //We are going to the make annotation linkset holding a set of references that is a dataset
    AnnotationLinkSet anotResult = domain.make(AnnotationLinkSet.class,"http://example.com/annotationResult");
    //Who was responsible for creating the links
    anotResult.setWasAttributedTo(jesse);
    //Which actvity created this annotation linkset
    anotResult.setWasGeneratedBy(annotationActivity);  
    //Which databases does it target
    anotResult.addTarget(ncbiGeneDatabase);
    anotResult.addTarget(ncbiProteinDatabase);
        
    //We create a article which we will be referencing
    //First create the authors of the paper
    Person person = domain.make(Person.class,"http://example.com/person1");
    person.setName("Ines Thiele");
    person.setGivenName("Ines");
    person.setSurName("Thiele");
    
    Person person2 = domain.make(Person.class,"http://example.com/person2");
    person2.setName("Bernhard Palsson");
    person2.setGivenName("Bernhard");
    person2.setSurName("Palsson");
    
    //Create the arctile itself
    AcademicArticle article = domain.make(AcademicArticle.class,"http://www.nature.com/nprot/journal/v5/n1/abs/nprot.2009.203.html");
    article.setUri("http://www.nature.com/nprot/journal/v5/n1/abs/nprot.2009.203.html");
    article.setPageStart("93");
    article.setPageEnd("121");
    article.setDoi("10.1038/nprot.2009.203");
    article.setLanguage("en");
    article.setTitle("A protocol for generating a high-quality genome-scale metabolic reconstruction");
    //Attach the authors
    article.addAuthorList(person);
    article.addAuthorList(person2);
    article.setAbstract("Network reconstructions are a common denominator in systems biology. Bottom–up metabolic network reconstructions have been developed over the last 10 years. These reconstructions represent structured knowledge bases that abstract pertinent information on the biochemical transformations taking place within specific target organisms. The conversion of a reconstruction into a mathematical format facilitates a myriad of computational biological studies, including evaluation of network content, hypothesis testing and generation, analysis of phenotypic characteristics and metabolic engineering. To date, genome-scale metabolic reconstructions for more than 30 organisms have been published and this number is expected to increase rapidly. However, these reconstructions differ in quality and coverage that may minimize their predictive potential and use as knowledge bases. Here we present a comprehensive protocol describing each step necessary to build a high-quality genome-scale metabolic reconstruction, as well as the common trials and tribulations. Therefore, this protocol provides a helpful manual for all stages of the reconstruction process.");
    
    //Attach an issue
    Issue issue = domain.make(Issue.class,"http://example.com/Issue1");
    issue.setVolume("5");
    issue.setIssue("1");
    issue.setCreated(LocalDate.of(2010,1,1));
    issue.addHasPart(article);
    
    //Attach the journal in which the issue is present
    Journal journal = domain.make(Journal.class,"http://example/Journal1");
    journal.setIssn("1754-2189");
    journal.setShortTitle("Nat. Protocols");
    journal.addHasPart(issue);
    
    //We create the element wise provenance, which in this case contains a note and a reference to the article
    ProvenanceAnnotation provAnnot = domain.make(ProvenanceAnnotation.class,"http://example.com/provannot");
    provAnnot.setProvenanceNote("The reference stated that is gene is performing the reaction X");
    provAnnot.addReference(article);
    
    //We create the provenance object that will link our xref and the existance of our features to the element and datawise provenance
    FeatureProvenance geneProv = domain.make(FeatureProvenance.class,"http://example.com/genProv");   
    geneProv.setOrigin(anotResult);
    geneProv.setAnnotation(provAnnot);
    //The default is to apply to all field including the existance so in this case that will be the existance and locustag
    //geneProv.addOnProperty("http://life.gbol/0.1/Existance");
    
    XRefProvenance xrefProv = domain.make(XRefProvenance.class,"http://example.com/xrefProv");   
    xrefProv.setOrigin(anotResult);
    xrefProv.setAnnotation(provAnnot);
    
    //We create a citation for the feature
    Citation citation = domain.make(Citation.class,"http://example.com/citation");
    citation.setReference(article);
    //Feature provenance is also a normal provenance
    citation.setProvenance(geneProv);
   
    XRef xref = domain.make(XRef.class,"http://example.com/axref");
    xref.setDb(ncbiGeneDatabase);
    xref.setAccession("106563252");
    xref.setProvenance(xrefProv);
    XRef xref2 = domain.make(XRef.class,"http://example.com/axref2");
    xref2.setDb(ncbiGeneDatabase);
    xref2.setAccession("XP_013984144.1");
    xref2.setProvenance(xrefProv);
    XRef goxref = domain.make(XRef.class,"http://example.com/goXRef");
    goxref.setDb(goDatabase);
    goxref.setAccession("0060092");
    goxref.setProvenance(xrefProv);
    
    Note note = domain.make(Note.class,"http://example.com/somenote");
    note.setText("This is the rate limiting step for pathway X");
    //Feature provenance is also a normal provenance
    note.setProvenance(geneProv);
  
    //Attach the provenance to the gene
    aGene.addProvenance(geneProv);
    //Add the xrefs, note and citation
    aGene.addNote(note);
    aGene.addXref(xref);
    aGene.addXref(xref2);
    aGene.addXref(goxref);
    aGene.addCitation(citation);
    //And finally add the gene as feature to the chromosome
    mainDnaObject.addFeature(aGene);
    
    //Now wrap everything into a GBOL dataset
    
    //Create the main GBOL dataset and linke the all the elements to it. No Sample is included in this example
    GBOLDataSet doc = domain.make(GBOLDataSet.class,"http://example.com/exampleGBOLDataset");
    doc.addAnnotationResults(anotResult);
    doc.addLinkedDataBases(ncbiGeneDatabase);
    doc.addLinkedDataBases(ncbiProteinDatabase);
    doc.addLinkedDataBases(goDatabase);
    doc.addSequences(mainDnaObject);
    
    domain.save("exampleresult.ttl");    
  }
  
  public static String checksum(String forCheckSum)
  {    
    try
    {
      MessageDigest md = MessageDigest.getInstance("SHA-384");
      md.update(forCheckSum.getBytes());
      byte[] digest = md.digest();
      StringBuffer sb = new StringBuffer();
      for (byte b : digest)
      {
        sb.append(String.format("%02x",b & 0xff));
      }      
      return (sb.toString());
    }
    catch (NoSuchAlgorithmException e)
    {
      e.printStackTrace();
    }
    return null;
  }
}
